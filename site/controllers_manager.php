<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 27.07.2018
 * Time: 9:50
 */

class controllers_manager
{
    private $contollers = array();

    function __construct()
    {

    }

    /**
     * @param $key
     * @return mixed
     */
    public function GetController($key)
    {
        if (array_key_exists($key, $this->contollers)) {
            return $this->contollers[$key];
        }
        echo "Контроллер не найден";
    }

    public function RegistrContoller($key, $controller)
    {
        $this->contollers[$key] = $controller;
    }
}