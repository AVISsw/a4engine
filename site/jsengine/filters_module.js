/**
 * Created by AVIS on 02.05.2018.
 */
window.onload = function (ev) {
    $.ajax({
        type: 'GET',
        url: '/handler/modules/catalog',
        data: 'filters=1',
        success: function (data) {
            var datafilters = JSON.parse(data);
            var typelist = [];
            try {
                for (var i = 0; i < datafilters.length; i++) {

                    typelist.push(datafilters[i]['type']);
                }
                typelist = unique(typelist);

            }catch (e) {
                console.log(e);
            }

            var filters = '';
            for (var i = 0; i < typelist.length; i++) {
                var type = typelist[i];
                var options = [];

                for(var j= 0;j < datafilters.length; j++){
                    var row = datafilters[j];
                    if(row["type"] == type) {
                        options.push(row["name"]);

                    }
                }
                filters += new_select(type,options);

                var x = $('#filters_c').html();
                if(x==type)
                {
                    $('#filters_c').html(new_select(type,options));
                }
            }


            $('#filters').html(filters);
            $('#cr_filters').html(new_datalist(type,typelist));

        }
    });
}