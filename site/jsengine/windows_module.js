/**
 * Created by AVIS on 26.07.2018.
 */
function show_pop_up_window_position(width,heigth,left,top,title_text,html){
    var popup_overlay = document.createElement("div");
    popup_overlay.className = 'popup_overlay';
    document.body.appendChild(popup_overlay);
    var elem = document.createElement("div");
    elem.className = 'popup';
    elem.style.width = width+'%';
    elem.style.height = heigth+'%';
    elem.style.top = top;
    elem.style.left = left;
    var title = document.createElement("div");
    title.className = 'popup_title';
    title.innerHTML = title_text;
    elem.appendChild(title);
    var close = document.createElement("span");
    close.className = 'closer';
    close.innerText = 'X';
    title.appendChild(close);
    var content = document.createElement("div");
    content.className = 'popup_content';
    content.innerHTML = html;
    elem.appendChild(content);
    document.body.appendChild(elem);
    $('.popup,.popup_overlay').fadeIn(400);
    var window = $('.popup,.popup_overlay');
    var closef = function () {
        console.log("close");
        $('.popup,.popup_overlay').fadeOut(400, function(){window.remove()});
      //  window.remove();
    }
    close.onclick = function(e){
        closef();
    }
    this.closef = closef;
}
function show_pop_up_window(width,heigth,title_text,html) {
   return show_pop_up_window_position(width,heigth,'50%','50%',title_text,html);
}
function pop_up_by_select(select,content) {
    pop_up_target($(select),content);
}
function pop_up_target(target,content) {
    var pos = target.offset();
    var elem = $("<div/>", {
        "class": "pop_up_target",
        html: content
    });

    var closef = function () {
        elem.css('display','none');
        elem.remove();
    }
    var close = $("<div/>", {
        "class": "closer",
        html: 'X',
        click: function () {
            closef();
        }
    });
    this.closef = closef;
    target.after(elem);
    elem.prepend(close);
    elem.offset(pos);
}