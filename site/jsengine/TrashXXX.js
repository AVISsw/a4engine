/**
 * Created by AVIS on 26.07.2018.
 */
function unique(arr) {
    var obj = [];

    for (var i = 0; i < arr.length; i++) {
        var str = arr[i];
        obj[str] = true; // запомнить строку в виде свойства объекта
    }

    return Object.keys(obj); // или собрать ключи перебором для IE8-
}

function option_list(list) {
    var result = '';
    for(var i =0;i<list.length;i++){
        result += "<option value='"+list[i]+"'>"+list[i]+"</option>";
    }
    return result;
}

function new_select(type,list) {
    var result_cr = "<label class='filter-head'>"+type+"</label><br><br><select multiple id='" +type + "-select' name='" + type + "[]' > ";
    result_cr += option_list(list);
    result_cr += "</select>";
    return result_cr;
}
function new_select_nm(type,list) {
    var result_cr = "<label class='filter-head'>"+type+"</label><br><select id='" +type + "-select' name='" + type + "'> ";
    result_cr += option_list(list);
    result_cr += "</select>";
    return result_cr;
}
function new_datalist(type,list) {
    var result_cr = "<label class='filter-head'>Категория</label><br><input list='cr_filters_type' name='type' id='cr_filters_input'><datalist id='cr_filters_type'>";
    result_cr += option_list(list);
    result_cr += "</datalist><br><label class='filter-head-value'>Значение</label><br><input type='text' name='name' id='type_name_input'>";
    return result_cr;
}

