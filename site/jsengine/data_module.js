/**
 * Created by AVIS on 26.07.2018.
 */

function FillElemAssoc(idContiner ,assoc_array) {
    $continer = $(idContiner);
    for(var key in assoc_array){
        var val = assoc_array[key];
        $continer.find('*[name='+key+']').html(val);
        console.log(key);
    }
    console.log($continer);
}
function FillElemAssocVal(idContiner ,assoc_array) {
    $(idContiner).each(function () {
       $(this).val('twe');
    });
}
function fill_pattern(contents ,assoc_array) {
    for(var key in assoc_array){
        var val = assoc_array[key];
        contents = contents.replace('{'+key+'/}',val);
    }
    return contents;
}

function FillElemJson(idContiner,jsonData) {
    var assoc_array = JSON.parse(jsonData);
    FillElemAssoc(idContiner, assoc_array);
}
function FillElemJsonVal(idContiner,jsonData) {
    var assoc_array = JSON.parse(jsonData);
    FillElemAssocVal(idContiner, assoc_array);
}
/*
Создает ассоциативный массив из дочерних элементов контейнера имеющих атрибут field
*/
function GetElemFields(idContiner) {
    result = [];
    var t = $(idContiner).find('*[field]');
    for (var i = 0; i < t.length; i++) {
        result[t[i].name] = t[i].value;
    }
    return result;
}

function GetElemData(selector) {
    var dat = $(selector).html();
    $(selector).html('');
    return dat;
}