<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 27.07.2018
 * Time: 9:47
 */

function sql_insert($post_arr, $table, $unset_arry)
{
    foreach ($unset_arry as $btn) {
        unset($post_arr[$btn]);
    }
    $sql_f = "";
    $sql_v = "";
    $arr_keys = array_keys($post_arr);
    foreach ($arr_keys as $key) {
        if (is_array($post_arr[$key])) {
            $items = '';
            foreach ($post_arr[$key] as $item) {
                $items .= $item . ";";
            }
            $sql_v .= "'" . $items . "',";
        } else {
            $sql_v .= "'" . $post_arr[$key] . "',";
        }
        $sql_f .= "`" . $key . "`,";

    }
    $result = "INSERT INTO `$table` (" . substr($sql_f, 0, -1) . ") VALUES (" . substr($sql_v, 0, -1) . ");";
    return $result;
}

function sql_update($post_arr, $table, $unset_arry, $cond_arr)
{
    foreach ($unset_arry as $btn) {
        unset($post_arr[$btn]);
    }
    $sql = "";
    $sql_v = "";
    $arr_keys = array_keys($post_arr);
    foreach ($arr_keys as $key) {
        if (is_array($post_arr[$key])) {
            $items = '';
            foreach ($post_arr[$key] as $item) {
                $items .= $item . ";";

            }
            $sql_v = $items;
        } else {
            $sql_v = $post_arr[$key];
        }

        $sql .= "`" . $key . "` = '" . $sql_v . "' ,";

    }
    $sql_cond = '';
    $cond_keys = array_keys($cond_arr);
    foreach ($cond_keys as $key) {
        $sql_cond .= "`" . $key . "` = '" . $cond_arr[$key] . "' AND ";
    }


    $result = "UPDATE `$table` SET " . substr($sql, 0, -1) . " WHERE " . substr($sql_cond, 0, -4);
    return $result;
}

function sql_select($post_arr, $table, $unset_arry)
{

    foreach ($unset_arry as $btn) {
        unset($post_arr[$btn]);
    }
    $sql = "";
    $arr_keys = array_keys($post_arr);
    foreach ($arr_keys as $key) {
        $sql .= "`" . $key . "` = '" . $post_arr[$key] . "' AND ";
    }
    $result = "SELECT * FROM `$table` WHERE " . substr($sql, 0, -4) . ";";
    return $result;
}