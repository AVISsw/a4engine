<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 13.11.2017
 * Time: 19:09
 * Это Routing класс он отвечает за получение путей и параметров для навигации
*/

    class Route
    {
        function __construct()
        {

        }

        function GetHost()
        {
            return "http://" . $_SERVER['HTTP_HOST'];
        }

        function GetParamsLine()
        {
            return $_SERVER['REQUEST_URI'];
        }


        /*
         * Нулевым индексом обладает дирректория main.php, все что до не не учитывается
         *
         * */

        function GetParamss()
        {
            // Имя папки в которой лежит main.php
            $dir = basename(__DIR__);
            $line = $this->GetHost().$this->GetParamsLine();
          //  echo $line;
            $line = substr($line,strlen(URI_APP));
         //   echo $line;
            $half_line = explode("?", $line);
            $path = explode("/",  $half_line[0]);
            array_shift($path);

            $res = new Result();
            $res->parmas = $_GET;
            $res->path = $path;
            return $res;
        }
    }

    class Result
    {
        public $parmas;
        public $path;
    }
