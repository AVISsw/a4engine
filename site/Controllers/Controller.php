<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 27.07.2018
 * Time: 10:09
 */

abstract class controller
{
    public $data = '';

    abstract public function init($config);

    public function draw($result)
    {
        $this->data = $result;
    }
    public function render(){
        echo  $this->data;
    }
}