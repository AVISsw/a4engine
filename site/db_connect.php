<?php

/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 27.09.2017
 * Time: 20:48
 */

class db_connect
{
    public  $connection;
    function __construct($hostname,$dbname,$username,$password){

        // Create connection
        $this->connection = new mysqli($hostname, $username, $password, $dbname);
        // Check connection
        if ($this->connection->connect_error) {
            die("Не удалось соединиться с базой: " . $this->connection->connect_error);
        }
        $this->connection->set_charset("utf8");
    }

}