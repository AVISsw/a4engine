<?php

/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 27.09.2017
 * Time: 22:31
 */


class page_builder extends controller
{
    public $registr = [];
    public function init($config)
    {

        ae_include('modules/module.php');
        $key = !empty($config->key) ? $config->key : 'catalog';
        $p_data = $this->get_page($config->connection,$key);// данные страницы
        $p_tmp = file_get_contents("html/head.html");
        $p_tmp .= file_get_contents(DIR_APP . 'html/' . $p_data['pattern']);// html шаблон
        $p_tmp .= file_get_contents("html/foot.html");
        $module_list = get_modules($p_tmp);
        $p_field_pattern = ['$$title$$', '$$DIR_APP$$',  '$$URI_APP$$'];// список полей страницы
        $p_field_value = [$p_data['name'], DIR_APP,  URI_APP];// значения полей страницы
        $onload = "";
        // механизм подключения модулей
        for ($i = 0; $i < count($module_list[1]); $i++) {
            $query = $module_list[1][$i];
            $p_field_pattern[] = "$$" . $query . "$$";
            $arg = parse_query($query);
            $config->onload = false;
            if($this->registr[$arg['name']] != true) {
                $this->registr[$arg['name']] = true;
                $config->onload = true;
            }
            $load = load_module($arg, $config);
            $p_field_value[] = @$load['data'];
            $onload .= @$load['onload'];
        }
        $p_tmp .=$onload;
        $this->draw(str_replace($p_field_pattern, $p_field_value, $p_tmp));// вывод страницы

    }


    // Функция получения данных о странице
    function get_page($connection,$key)
    {
        $sql = $connection->query("SELECT * FROM `pages` WHERE `key`= '" . $key . "';");
        if ($result = $sql->fetch_assoc()) {
            return $result;
        }
        return null;
    }

}