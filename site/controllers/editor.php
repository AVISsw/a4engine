<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 06.08.2018
 * Time: 10:05
 */
class editor extends controller {

    public function init($config)
    {
        ae_include('modules/module.php');
        ae_include('modules/editor_module.php');
        $config->module_name = $config->key;
        $config->editor_name = $config->routing->path[2];
        // подключаем класс модуля и создаем объект модуля
        $path = DIR_APP . "modules/$config->module_name/editor/";
        $file = $path . "$config->editor_name.php";

        if (file_exists($file)) {
            require_once $file;
            $module = new $config->editor_name();
            if ($module instanceof editor_module) {
                $module->before($config);
                $module->start($config);
                $module_value = $module->result_data;
                $this->draw($module_value);
            } else {
                $this->draw( '' . $config->module_name . ' не реалезует module <br>');
            }
        } else {
            $this->draw("Редактор $config->module_name -> $config->editor_name не найден <br>");
        }

    }
}