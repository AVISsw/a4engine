<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 */
class handler_manager extends controller
{
    public function init($config)
    {
        ae_include('modules/handler.php');
        $config->module_name = $config->routing->path[1];
        $handler_name =  $config->routing->path[2];
        // подключаем класс модуля и создаем объект модуля
        $config->path = DIR_APP."modules/$config->module_name/";
        $file = $config->path . "$handler_name.php";
        if (file_exists($file)) {
            require_once $file;
            $module = new $handler_name();

            if ($module instanceof handler) {
                $module->before($config);
                $module->start();
                $module_value = $module->result_data;
                $this->draw($module_value);
            } else {
                //исключение или обработка ошибки
            }
        } else {
            $this->draw('Обработчик ' . $config->module_name . '->'.$handler_name.' не найден <br>');
        }
    }
}
