<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 27.09.2017
 * Time: 20:44
 */


define('DIR_APP',dirname(__FILE__).'/');
define('URI_APP', 'http://'.$_SERVER['HTTP_HOST'].'/'.substr(dirname($_SERVER['PHP_SELF']),1));

ae_include('routing.php');
ae_include('addodition.php');
ae_include('sql_helper.php');
ae_include('controllers_manager.php');
ae_include('controllers/controller.php');
ae_include('controllers/page_builder.php');
ae_include('controllers/handler_manager.php');
ae_include('controllers/editor.php');
ae_include('controllers/peditor.php');
ae_include('db_connect.php');
ae_include('debug.php');
ae_include('modules/component.php');

$page_config = parse_ini_file( DIR_APP.'config.ini' );
ini_set('display_errors',$page_config['display_errors']);
$connection = new db_connect($page_config["hostname"],$page_config["dbname"],$page_config["username"],$page_config["password"]);
$connection = $connection->connection;
$routing = new route();
$route = $routing->GetParamss();
$controllerConfig = [];
$controllerManager = new controllers_manager();
$controllerManager->RegistrContoller("page",new page_builder());
$controllerManager->RegistrContoller("handler",new handler_manager());
$controllerManager->RegistrContoller("editor",new editor());
$controllerManager->RegistrContoller("peditor",new peditor());
$controller = $controllerManager->GetController($route->path[0]);

$controllerConfig['connection'] = $connection;
$controllerConfig['routing'] = $route;
$controllerConfig['key'] = $route->path[1];
$controllerConfig['user'] = get_user_data($connection);

$controller->init(array_to_object($controllerConfig));
$controller->render();

$_XSS_GET = $_GET;
$_XSS_POST = $_POST;
//$_GET = Addodition\defender_xss($connection,$_GET);
//$_POST = Addodition\defender_xss($connection,$_POST);




function ae_include($src)
{
    $path = DIR_APP . $src;
    if (file_exists($path)) {
        include_once($path);
    } else {
        echo "Фаил " . $src . " не найден";
    }
}




