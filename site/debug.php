<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 30.07.2018
 * Time: 9:07
 */
class debug
{
    public static $logs = [];
    public static function log($info)
    {
        debug::outup($info,'log');
    }

    public static function info($info)
    {
        debug::outup($info,'info');
    }

    public static function warn($info)
    {
        debug::outup($info,'warn');
    }

    public static function error($info)
    {
        debug::outup($info,'error');
    }
    public static function outup($info,$type){
        $data['info'] = $info;
        $data['type'] = $type;
        $date = new DateTime();
        $result = $date->format('Y-m-d H:i:s');
        $data['date'] =  $result;
        debug::$logs[] = $data;
    }
}