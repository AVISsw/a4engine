<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 29.07.2018
 * Time: 14:44
 */

class catalog extends module
{
    public $is_first_filter = true;

    public function start($arg)
    {
        $conn = $this->data->connection;
        $result = "";
        $data = $this->get_search_data($conn);
        $data = isset($data) ? $data : [];
        foreach ($data as $row) {
            $result .= fill_pattern($this->data->dir_module . 'html/', 'form.html', $row);
        }
        $fill_data = $this->data->fill_data;
        $fill_data['body'] = $result;
        $result = fill_pattern($this->data->dir_module,'html/catalog.html',$fill_data);
        $this->draw($result);
    }

    function get_search_data($conn)
    {
        $result = [];
        $query_text = 'SELECT * FROM `catalog` '; // базовый запрос
        array_shift($_GET); // убюираем первйы гет т.к. это путь до скрипта
        $query_text .= count($_GET) > 0 ? 'WHERE ' : ''; // если есть гет параметры
        // мульти выбор
        foreach (array_keys($_GET) as $item) {
            $value = $_GET[$item];
            $items = '';
            if (is_array($value)) {
                foreach ($value as $item2) {
                    $items .= " `$item` LIKE '%$item2%' or ";
                }
                $items = mb_substr($items, 0, -3);
            } else {
                $items = "`$item`='$value'";
            }
            $query_text .= $this->add_filter($items);
        }
        //      $query_text .= " ORDER BY `check` DESC ";

        debug::info($query_text);
        $sql = $conn->query($query_text);
        while ($row = $sql->fetch_array()) {
          //  $row["body"] = mb_strimwidth($row["body"], 0, 160) . "...";
            $row = array_merge($row, $this->data->fill_data);
            $postes[] = $row;
        }
        return $postes;
    }

    function add_filter($query)
    {
        $result = '';
        if ($this->is_first_filter) {
            $result = $query;
            $this->is_first_filter = false;
        } else {
            $result = ' AND ' . $query;
        }
        return $result;
    }
}