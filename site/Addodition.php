<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 27.09.2017
 * Time: 20:47
 */

    function is_vlid($value)
    {
        if (strlen($value) > 0) {
            return true;
        }
        return false;
    }


// принимает имя шаблона и асоциативный массив
    function fill_pattern($path, $pattern_name, $m_data)
    {
        $p_tmp = file_get_contents($path . $pattern_name);
        $field_list = get_modules($p_tmp);

        $p_field_pattern = array();// список полей страницы
        $p_field_value = array();// значения полей страницы

        $params = array();


        for ($i = 0; $i < count($field_list[1]); $i++) {
            $field_name = $field_list[1][$i];
            $p_field_pattern[] = trim("$$" . $field_name . "$$");
            $p_field_value[] = $m_data[$field_name];
        }
        return str_replace($p_field_pattern, $p_field_value, $p_tmp);
    }

// список модулей
    function get_modules($pattern)
    {
        preg_match_all("/\\$\\$(.*?)\\$\\$/", $pattern, $matches);
        return $matches;
    }

    function get_table($data, $row_pattern, $table)
    {
        $temp = '';
        $params = array();
        $rows = '';
        if (is_array($data)) {
            foreach ($data as $row) {
                $rows .= fill_pattern('', $row_pattern, $row);
            }
        }
        $params['rows'] = $rows;
        $temp = fill_pattern('', $table, $params);
        return $temp;
    }

    function get_table_from_key($data, $row_pattern)
    {
        $rows = '';
        if (is_array($data)) {
            foreach ($data as $row) {
                $rows .= fill_pattern('', $row_pattern, $row);
            }
        }
        return $rows;
    }

    function parse_query($query)
    {
        $arg = explode(" ", $query);
        $result = array();
        $result['type'] = $arg[0];
        foreach ($arg as $line) {
            $line = explode("=", $line);
            $result[$line[0]] = $line[1];
        }
        return $result;
    }

    function get_status($user_data, $status)
    {
        if ($user_data['status'] >= $status) {
            return true;
        }
        return false;
    }

    function load_module($arg,$config)
    {
        $config->module_name = $arg['name'];
        if ($arg['type'] != 'module')
            return '';
        // подключаем класс модуля и создаем объект модуля
        $config->path = DIR_APP . "modules/$config->module_name/";
        $file = $config->path . "$config->module_name.php";
        if (file_exists($file)) {
            require_once $file;
            $module = new $config->module_name();
            if ($module instanceof module) {
                $module->before($config,$arg);
                $module->start($arg);
                $module_value['data'] = $module->result_data;
                if($config->onload == true) {
                    $module_value['onload'] = $module->onload();
                }
                return $module_value;
            } else {
                return '' . $config->module_name . ' не реалезует module <br>';
            }
        } else {
            return 'Модуль ' . $config->module_name . ' не найден <br>';
        }
    }

    function defender_xss($connection, $arr)
    {

        foreach ($arr as $num => $xss) {
            if (is_array($xss)) {
                foreach ($xss as $xsss) {
                    strip_tags(mysqli_real_escape_string($connection, $xsss), ENT_QUOTES);
                }
            } else {
                $arr[$num] = strip_tags(mysqli_real_escape_string($connection, $xss), ENT_QUOTES);
            }
        }
        return $arr;
    }

    function get_status_value($conn)
    {
        $query = $conn->query("SELECT * FROM `accounts` WHERE  `session`='" . $_COOKIE["sessionHash"] . "'");
        $result = $query->fetch_object();
        return $result->status;
    }

    function get_user_data($conn)
    {
        $query = $conn->query("SELECT * FROM `accounts` WHERE  `session`='" . $_COOKIE["sessionHash"] . "'");
        $result = $query->fetch_assoc();
        return $result;
    }

    function array_to_object($array){
        return (object)$array;
    }