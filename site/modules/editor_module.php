<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 07.08.2018
 * Time: 19:19
 */
abstract class editor_module extends module{

    public function before($config){
        parent::before($config,'');
        $this->data->uri_editor_module = URI_APP.'/modules/'.$config->module_name.'/editor';
        $this->data->dir_editor_module = DIR_APP.'/modules/'.$config->module_name.'/editor';
        $this->data->fill_data['uri_editor_module'] = $this->data->uri_editor_module;
        $this->data->fill_data['dir_editor_module'] = $this->data->dir_editor_module;
    }
}