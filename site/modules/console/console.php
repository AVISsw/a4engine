<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 29.07.2018
 * Time: 14:44
 */

class console extends module{

    public function start($arg)
    {

        if(get_status($this->data->user,1)) {
            $data = "";

            foreach (debug::$logs as $row) {
                $data .= fill_pattern($this->data->dir_module, 'html/row.html', $row);
            }

            $result["body"] = $data;
            $result = array_merge($result, $this->data->fill_data);
            $this->draw(fill_pattern($this->data->dir_module, 'html/form.html', $result));
        }
    }
}