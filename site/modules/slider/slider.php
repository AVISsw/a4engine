<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 08.09.2018
 * Time: 11:52
 */

class slider extends component
{

    public function start($arg)
    {
        $data = [];
        $images = '';
        $tumbler = '';
        $slides = $this->get_data_slider($arg['key'],$this->data->connection);
        foreach ($slides as $row) {
            $id = $row['id'];
            $uri = URI_APP. $row['uri'];
            $images .= " <li id=\"photo-$id\"><a href=\"#p\"><img src=\"$uri\" class='photo' /></a></li>";
            $tumbler .= "<li id=\"thumb-$id\"><a href=\"#t1\"><img src=\"$uri\" class='tumbler' /></a></li>";
        }
        $this->data->fill_data['images'] = $images;
        $this->data->fill_data['tumbler'] = $tumbler;
        $this->draw(fill_pattern($this->data->dir_component, '/html/form.html',$this->data->fill_data));

    }
    function get_data_slider($key,$conn){
        $sql = $conn->query("SELECT * FROM `slider` WHERE `key`='$key';");
        $data = [];
        while ($row = $sql->fetch_array()) {
            $row['handler'] = URI_APP.'/page/catalog';
            $data[] = $row;
        }
        return $data;
    }
}