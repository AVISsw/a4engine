<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 29.07.2018
 * Time: 15:56
 */
abstract class handler
{
    public $data;

    public function before($config)
    {
        $this->data = $config;
        $this->data->dir_handler = $config->path;
        $this->data->uri_module = URI_APP.'/modules/'.$config->module_name;
        $this->data->uri_handler = URI_APP.'/handler/'.$config->module_name;
        $this->data->fill_data = ['dir_module'=>$this->data->dir_module , 'uri_module'=>$this->data->uri_module,'DIR_APP'=>DIR_APP,'URI_APP'=>URI_APP,'uri_handler'=>$this->data->uri_handler];
        $this->data->config = file_exists($this->data->dir_handler . 'config.ini') ? parse_ini_file($this->data->dir_handler . 'config.ini'): [];
    }

    // вызывается при создании экземпляра
    abstract public function start();

    public function data_return($result)
    {
        $this->result_data = $result;
    }
}