<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 22.11.2017
 * Time: 12:59
 */
class menu extends module
{

    public function start($arg)
    {
        $conn = $this->data->connection;
        $key = $arg['key'];
        $is_adm = get_status($this->data->user, 777);
        $menuItems = $this->get_data($key, $conn);
        $result = "";
        // цикл собирает строку из постов
        for ($i = 0; $i < count($menuItems); $i++) {
            $menu = $menuItems[$i];
            $linksData = $this->get_links($conn, $menu["mkey"]);
            $links = $this->build_links($linksData, $is_adm);
            $menu["links"] = $links;
            $menu = array_merge($menu, $this->data->fill_data);
            if ($is_adm) {
                $menu["control"] = fill_pattern($this->data->dir_module, 'editor/html/adm_menu.html', $menu);
            }
            $result .= fill_pattern($this->data->dir_module . "html/", $menu["pattern"], $menu);
        }
        $this->draw($result);
    }


    public function onload()
    {
        return fill_pattern($this->data->dir_module,'editor/html/onload.html',$this->data->fill_data);
    }

    public function get_data($key, $conn)
    {
        $sql = $conn->query("SELECT * FROM `menu` WHERE `key`= '$key';");
        while ($row = $sql->fetch_assoc()) {
            $menuItems[] = $row;
        }
        return $menuItems;
    }

    function get_links($conn, $key)
    {

        $sql = $conn->query("SELECT * FROM `links` WHERE `key`= '$key';");
        while ($row = $sql->fetch_assoc()) {
            $row['link'] = $row['islocal'] == 1 ? URI_APP . '/' . $row['link'] : $row['link'];
            $menuItems[] = $row;
        }
        return $menuItems;
    }

    function build_links($links, $isAdm)
    {
        $result = "";
        foreach ($links as $link) {
            $l_pattern = '/' . $link["pattern"];
            $link = array_merge($link, $this->data->fill_data);
            if ($isAdm) {
                $link["control"] = fill_pattern($this->data->dir_module, "editor/html/control.html", $link);
            }
            $result .= fill_pattern($this->data->dir_module . 'html/', $l_pattern, $link);
        }
        return $result;
    }
}