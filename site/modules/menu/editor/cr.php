<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 01.12.2017
 * Time: 21:52
 */

class cr extends editor_module
{

    public function start($arg)
    {

        if (get_status($this->data->user, 777)) {
            $text = sql_insert($_POST,'links',['send']);
            $query = $this->data->connection->query($text);
            $editor = 'Ссылка создана '.$text;
        } else {
            $editor = 'Ошибка при создании';
        }
        $editor .= '<br><a href="'.URI_APP.'/page">На главную</a>';
        $this->draw($editor);
    }
}
