<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 03.12.2017
 * Time: 22:13
 */


class rm extends editor_module
{
    public function start($arg)
    {
        $id = $_GET['id'];
        if (get_status($this->data->user, 777)) {
            $query = $this->data->connection->query("DELETE FROM `links` WHERE `id`= $id;");
            $editor = 'Ссылка '.$id.' удалена';
        } else {
            $editor = 'у вас недостаточно прав';
        }
        $editor .= '<br><a href="'.URI_APP.'/page">На главную</a>';
        $this->draw($editor);
    }
}
