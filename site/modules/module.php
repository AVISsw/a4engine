<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 03.10.2017
 * Time: 13:20
 */
abstract class module  {

    public $data;

    public function before($config,$arg){
        $this->data = $config;
        $this->data->dir_module = $config->path;
        $this->data->uri_module = URI_APP.'/modules/'.$config->module_name;
        $this->data->uri_handler = URI_APP.'/handler/'.$config->module_name;
        $this->data->fill_data = ['dir_module'=>$this->data->dir_module , 'uri_module'=>$this->data->uri_module,'DIR_APP'=>DIR_APP,'URI_APP'=>URI_APP,'uri_handler'=>$this->data->uri_handler];
        $this->data->config = file_exists($this->data->dir_module . 'config.ini') ? parse_ini_file($this->data->dir_module . 'config.ini'): [];

    }

    // вызывается при создании экземпляра модуля
    abstract public function start($arg);

    /*
    * Вызывается для загрузки общих ресурсов модуля, всех его экземпляров
    * */
    public function onload(){

    }

    public function draw($result)
    {
        $this->result_data = $result;
    }
}