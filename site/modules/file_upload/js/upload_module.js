var form_data = new FormData();
var fileobj;

function upload_file(e) {
    e.preventDefault();
    fileobj = e.dataTransfer.files[0];
    ajax_file_upload(fileobj);
}

function file_explorer() {
    document.getElementById('selectfile').click();
    document.getElementById('selectfile').onchange = function () {
        fileobj = document.getElementById('selectfile').files[0];
        ajax_file_upload(fileobj);
    };
}

var upload_result = function (data) {
    
}

var updateajaxform = function () {

}

function ajax_file_upload(file_obj) {
    if (file_obj != undefined) {
        form_data = new FormData();
        form_data.append('file', file_obj);
        form_data.append('upload', 1);
        $.ajax({
            type: 'POST',
            url: '/handler/modules/upload',
            contentType: false,
            processData: false,
            data: form_data,
            success: function (response) {
                $('#selectfile').val('');
                upload_result(response);
                updateajaxform();
            }
        });
    }
}