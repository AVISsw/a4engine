<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 02.08.2018
 * Time: 22:00
 */
abstract class component
{
    public $data;
    public $result;

    public function __construct($config, $arg)
    {
        $this->data = $config;
        $this->data->uri_component =  URI_APP . '/modules/' .get_class($this);
        $this->data->dir_component = DIR_APP . 'modules/' . get_class($this);
        $this->data->uri_handler = URI_APP.'/handler/'.get_class($this);
        $this->data->fill_data = ['dir_component'=>$this->data->dir_component , 'uti_component'=>$this->data->uti_component,'DIR_APP'=>DIR_APP,'URI_APP'=>URI_APP,'uri_handler'=>$this->data->uri_handler];
        $this->data->fill_data['uri_component'] = $this->data->uri_component;
        $this->data->fill_data['dir_component'] = $this->data->dir_component;
        $this->start($arg);
    }

    // вызывается при создании экземпляра компонента
    abstract public function start($arg);

    function draw($data)
    {
        $this->result .= $data;
    }

    public function get_data()
    {
        return $this->result;
    }
}