/**
 * Created by AVIS on 06.08.2018.
 */

$(".catalogbox").mouseenter(function() {
    var el = $(this);
    el.find('.catalogboxdesc').fadeIn(500);
    el.mouseleave(function() {
        el.find('.catalogboxdesc').fadeOut(500);
    });
});
