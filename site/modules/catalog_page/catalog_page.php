<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 22.11.2017
 * Time: 12:10
 */
class catalog_page extends module{

    public function start($arg)
    {
        ae_include('modules/slider/slider.php');
        $id = $this->data->routing->path[2];
        $key = $this->data->routing->path[1];
        $arg['key'] = $key;
        $html= fill_pattern($this->data->dir_module,"buttons.html",array("id"=>$id));
        $data = array_merge($this->get_data($id,$this->data->connection),$this->data->fill_data);
        $slider = new slider($this->data,$arg);
        $data['slider'] = $slider->get_data();
        if(get_status($this->data->user,1)) {
            $data['res'] = '<a href="#" id="job_form" class="button_hover theme_btn_two">Забронировать номер</a>';
        }
        $html .=fill_pattern($this->data->dir_module,"form.html",$data);
        $this->draw($html);
    }

    function get_data($id,$conn)
    {
        $sql = $conn->query("SELECT * FROM `catalog` WHERE `id`=$id;");
        if ($row = $sql->fetch_array()) {
            $row['handler'] = URI_APP.'/page/catalog';
            return $row;
        }
    }

}