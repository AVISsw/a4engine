<?php
/**
 * Created by PhpStorm.
 * User: AVIS
 * Date: 30.11.2017
 * Time: 18:42
 */
class login extends module{

    public function start($routing)
    {

        $content = fill_pattern( $this->data->dir_module,'form.html',$this->data->fill_data);
        if(get_status($this->data->user,1)){
            $data = get_user_data($this->data->connection);
            $data = $data + $this->data->fill_data;
            $content .= fill_pattern($this->data->dir_module,'user_box.html',$data);
        }
        $this->draw($content);
    }
}