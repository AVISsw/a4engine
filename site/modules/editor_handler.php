<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 07.08.2018
 * Time: 18:43
 */

abstract class editor_handler extends handler{

    public function before($config){
        parent::before();
        $this->data->uri_editor_handler = URI_APP.'/modules/'.$config->module_name.'/editor';
        $this->data->dir_editor_handler = $config->path.'/editor';
        $this->data->fill_data['uri_editor_handler'] = $this->data->uri_editor_handler;
        $this->data->fill_data['dir_editor_handler'] = $this->data->dir_editor_handler;

    }

}
